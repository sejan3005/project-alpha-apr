from django.urls import path
from .views import item_projects

urlpatterns = [
    path("projects", item_projects, name="list_projects"),
]
